<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, maximum-scale=1">
  <title>Love Pet</title>
  <link rel="icon" href="favicon.png" type="image/png">
  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="css/animate.css" rel="stylesheet" type="text/css">
</head>

<body>

  <!--Header_section-->
  <header id="header_wrapper">
    <div class="container">
      <div class="header_box">
        <div class="logo"><a href="#"><img height="50px" width="150px" src="img/cor/logo.png" alt="logo"></a></div>
        <nav class="navbar navbar-inverse" role="navigation">
          <div class="navbar-header">
            <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
          <div id="main-nav" class="collapse navbar-collapse navStyle">
            <ul class="nav navbar-nav" id="mainNav">
              <li class="active"><a href="#hero_section" class="scroll-link">Home</a></li>
              <li><a href="#service" class="scroll-link">Services</a></li>
              <li><a href="#clients" class="scroll-link">Downloads</a></li>
              <li><a href="#subscriptionForm" class="scroll-link">Subscription</a></li>
              <li><a href="#contact" class="scroll-link">Contact</a></li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </header>
  <!--Header_section-->

  <!-- carousel -->
  <section id="hero_section">

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" style="height: 761px;">
        <div class="item active">
          <img src="img/cor/cor4.png" alt="Los Angeles">
        </div>

        <div class="item">
          <img src="img/cor/cor2.jpg" alt="Chicago">
        </div>

        <div class="item">
          <img src="img/cor/cor3.jpg" alt="New York">
        </div>

        <div class="item">
          <img src="img/cor/cor1.jpg" alt="New York">
        </div>
      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </section>
  <!-- carousel -->

  <!--Service-->
  <section id="service">
    <div class="container">
      <h2>Services</h2>
      <div class="service_wrapper">
        <div class="row">
          <div class="col-lg-4">
            <div class="service_block">
              <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="fa fa-paw"></i></span> </div>
              <h3 class="animated fadeInUp wow">Vet Appointment & Schedule</h3>
              <p class="animated fadeInDown wow" style="text-align: justify;">LovePet app allows you to schedule your preferred appointment time by showing the Appointment Booking Calendar. This will help you to see your Vet Dairy and you can then book your appointment accordingly.
In case either you or VET cancel the scheduled appointment then the App is smart enough to provide best available time for the appointment.</p>
            </div>
          </div>
          <div class="col-lg-4 borderLeft">
            <div class="service_block">
              <div class="service_icon icon2  delay-03s animated wow zoomIn"> <span><i class="fa fa-paw"></i></span> </div>
              <h3 class="animated fadeInUp wow">Pet Matting Request</h3>
              <p class="animated fadeInDown wow" style="text-align: justify;">LovePet app provide a best mating platform for your Pet. You can simply search any suitable mating partner for your Pet with simple search criteria. 
If you are looking to post your PET for mating then your request will be accepted in few hours and your PET will be available in Search result.
A Simple few step process make your and your Pet’s life simple.</p>
            </div>
          </div>
          <div class="col-lg-4 borderLeft">
            <div class="service_block">
              <div class="service_icon icon3  delay-03s animated wow zoomIn"> <span><i class="fa fa-paw"></i></span> </div>
              <h3 class="animated fadeInUp wow">Loyalty Program</h3>
              <p class="animated fadeInDown wow" style="text-align: justify;">Lovepet App provides facilities to all the VET to assign or create a Loyalty Program for their PET Parents. Pet Parent can see the Loyalty program assigned to them by their VET and then utilise points those been earned by availing services.
How does it work? Your VET create a Loyalty Program with Points and then same program can be seen into Pet Parent dashboard , you can then avail those services and earn points</p>
            </div>
          </div>
        </div>
        <div class="row borderTop">
          <div class="col-lg-4 mrgTop">
            <div class="service_block">
              <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="fa fa-paw"></i></span> </div>
              <h3 class="animated fadeInUp wow">Pet Fostering</h3>
              <p class="animated fadeInDown wow" style="text-align: justify;">If you are unable to carry your PET in your Holidays then do not worry, LovePet app provides the best foster case platform to find out professional Fostering service in your preferred area. 
You just need to login to app and find out the fostering services provided by fostering partner in your preferred area.</p>
            </div>
          </div>
          <div class="col-lg-4 borderLeft mrgTop">
            <div class="service_block">
              <div class="service_icon icon2  delay-03s animated wow zoomIn"> <span><i class="fa fa-paw"></i></span> </div>
              <h3 class="animated fadeInUp wow">Tip's From Vet</h3>
              <p class="animated fadeInDown wow" style="text-align: justify;">LovePet app provides the best tips for your pet ,  expert Doctors post their blogs and training materials which can be accessed by PET Parents.
Lovepet app also provide the Consultation on request so you can not only read tips or blogs but you can discuss with experts on various topics. </p>
            </div>
          </div>
          <div class="col-lg-4 borderLeft mrgTop">
            <div class="service_block">
              <div class="service_icon icon3  delay-03s animated wow zoomIn"> <span><i class="fa fa-paw"></i></span> </div>
              <h3 class="animated fadeInUp wow">Pet Store</h3>
              <p class="animated fadeInDown wow" style="text-align: justify;">Get a complete e-commerce store for your PET products. LovePet app provide you a high quality shopping experience for your Pet’s accessories and food. We have also managed to add Medicines in product category so you can purchase/ Reserve them from your VET and collect at VET Clinic. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--Service-->

  <section class="page_section" id="clients">
    <!--page_section-->
    <h2>Downloads</h2>
    <!--page_section-->
    <div class="client_logos">
      <!--client_logos-->
      <div class="container">
        <ul class="fadeInRight animated wow">
          <li><a class="mobileApp" href="javascript:void(0)"><img src="img/client_logo1.png" alt=""></a></li>
          <li><a class="mobileApp" href="javascript:void(0)"><img src="img/client_logo2.png" alt=""></a></li>
        </ul>
      </div>
    </div>
  </section>
  <!--client_logos-->


  <!-- Subscription from -->
  <hr>
  <div class="container">
    <section class="page_section contact" id="subscriptionForm">
      <div class="contact_section">
        <h2 style="color: black;">Subscription</h2>
        <div class="row">
          <div class="col-lg-4">

          </div>
          <div class="col-lg-4">

          </div>
          <div class="col-lg-4">

          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-2 wow fadeInLeft">
        </div>
        <div class="col-lg-8 wow fadeInLeft delay-06s">
          <div class="form">
            <form action="" method="POST" id="subscribe">
              <input class="input-text" type="text" required name="fullNameSub" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;" id="fullNameSub" placeholder="Full Name">
              <input class="input-text" type="email" required name="emailSub" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;" id="emailSub" placeholder="Email">
              <select class="input-text" required name="countryCodeSub" id="countryCodeSub" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <option value="+91">+91 India</option>
                <option value="+01">+01 USA</option>
                <option value="+44">+44 UK</option>
              </select>
              <input class="input-text" type="text" maxlength="10" pattern="\d{3}[\-]\d{3}[\-]\d{4}" required name="phoneSub" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;" id="phoneSub" placeholder="Phone">
              <select class="input-text" required name="subTypeSub" id="subTypeSub" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">
                <option value="">Select Subscription Type</option>
                <option value="Free Trial">Free Trial</option>
                <option value="1 Month">1 Month</option>
                <option value="6 Months">6 Months</option>
                <option value="12 Months">12 Months</option>
              </select>
              <textarea class="input-text text-area" required name="messageSub" cols="0" rows="0" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;" id="messageSub" placeholder="Message"></textarea>
              <input class="btn btn-primary" type="submit" name="submitSub" value="Subscribe">
            </form>
            <?php
            if (isset($_POST['submitSub'])) {
              $to = "Info@RajyugSolutions.com"; // this is your Email address
              $from = $_POST['emailSub']; // this is the sender's Email address
              $name = $_POST['fullNameSub'];
              $subject = "Subscription To LovePet";
              $subject2 = "Copy of your form submission";
              $message = $name . " wrote the following:" . "\n\n" . $_POST['messageSub'] . "Subscription Type: " . $_POST['subTypeSub'] . "Mobile No: " . $_POST['countryCodeSub'] . "-" . $_POST['phoneSub'];
              $message2 = "Here is a copy of your message " . $name . "\n\n" . $_POST['messageSub'];

              $headers = "From:" . $from;
              $headers2 = "From:" . $to;
              mail($to, $subject, $message, $headers);
              mail($from, $subject2, $message2, $headers2);
              echo '<p class="text-success mt-4">Mail Sent. Thank you ' . $name . ', we will contact you shortly.</p>';
            }
            ?>
          </div>
        </div>
      </div>
    </section>
  </div>

  <!--Footer-->
  <footer class="footer_wrapper" id="contact">
    <div class="container">
      <section class="page_section contact" id="contact">
        <div class="contact_section">
          <h2>Contact Us</h2>
          <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">

            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 wow fadeInLeft">
            <div class="contact_info">
              <div class="detail">
                <h4>Office</h4>
                <p>Rajyug Solutions Ltd Company Reg. No: 11583897
                  265-269 Kingston Road, London, United Kingdom</p>
              </div>
              <div class="detail">
                <h4>Call Us</h4>
                <p>(UK) +44 20 3603 7797</p>
                <p>(India) +91 91 3008 9797</p>
              </div>
              <div class="detail">
                <h4>Email Us</h4>
                <p>Info@RajyugSolutions.com</p>
              </div>
            </div>


          </div>
          <div class="col-lg-8 wow fadeInLeft delay-06s">
            <div class="form">
              <form action="" method="POST" id="contact">
                <input class="input-text" type="text" name="name" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;" id="name" placeholder="Name">
                <input class="input-text" type="text" name="email" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;" id="email" placeholder="Email">
                <textarea class="input-text text-area" name="message" cols="0" rows="0" onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;" id="message" placeholder="Message"></textarea>
                <input class="input-btn" type="submit" name="submit" value="send message">
              </form>
              <?php
              if (isset($_POST['submit'])) {
                $to = "Info@RajyugSolutions.com"; // this is your Email address
                $from = $_POST['email']; // this is the sender's Email address
                $name = $_POST['name'];
                $subject = "Form submission";
                $subject2 = "Copy of your form submission";
                $message = $name . " wrote the following:" . "\n\n" . $_POST['message'];
                $message2 = "Here is a copy of your message " . $name . "\n\n" . $_POST['message'];

                $headers = "From:" . $from;
                $headers2 = "From:" . $to;
                mail($to, $subject, $message, $headers);
                mail($from, $subject2, $message2, $headers2);
                echo '<p class="text-success mt-4">Mail Sent. Thank you ' . $name . ', we will contact you shortly.</p>';
              }
              ?>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="container">
      <div class="footer_bottom">
        <p class="copyright pt20 pb0 text-center">
          This website is made with <i class="fas fa-heart"></i> by <a href="http://rajyugsolutions.com/" data-slimstat="5" target="_blank"> RajYug It Solutions Pvt Ltd</a></p>
      </div>
    </div>
  </footer>

  <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
  <script type="text/javascript" src="js/jquery.nav.js"></script>
  <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="js/jquery.isotope.js"></script>
  <script type="text/javascript" src="js/wow.js"></script>
  <script type="text/javascript" src="js/custom.js"></script>
  <script type="text/javascript">
    $('#contact').submit((e) => {
      if (!$('#name').val() || !$('#email').val() || !$('#message').val()) {
        alert('Form can not be empty!');
        return false;
      } else {
        return true;
      }
    })

    $('#subscribe').submit((e) => {
      if (!$('#fullNameSub').val() || !$('#emailSub').val() || !$('#countryCodeSub').val() || !$('#phoneSub').val() || !$('#subTypeSub').val() || !$('#messageSub').val()) {
        alert('Form can not be empty!');
        return false;
      } else {
        return true;
      }
    })

    $('.mobileApp').click(() => {
      alert('Mobile App will launch shortly!');
    })
  </script>
</body>

</html>